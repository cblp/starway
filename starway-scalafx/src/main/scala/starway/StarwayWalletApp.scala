package starway

import scalafx.application.JFXApp3
import scalafx.application.JFXApp3.PrimaryStage
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.{Accordion, Tab, TabPane, TextField, TitledPane}
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.HBox
import scalafx.scene.paint.{Color, LinearGradient, Stops}
import scalafx.scene.text.Text

import scala.language.implicitConversions

object StarwayWalletApp extends JFXApp3:

  override def start(): Unit =
    stage = new PrimaryStage:
      title = "Starway Wallet"
      scene = new Scene:
        root = new TabPane:
          tabs = Seq(
            new Tab:
              text = "Tools"
              closable = false
              content = new Accordion:
                panes = Seq(
                  new TitledPane:
                    text = "Vanity Address Generator"
                    content = new TextField
                )
          )
