{-# LANGUAGE OverloadedLists #-}

-- import GI.Gtk (AttrOp ((:=)), new, on, set)
import GI.Gtk qualified as Gtk
import GI.Gtk.Declarative (Attribute ((:=)), bin, on, widget)
import GI.Gtk.Declarative.App.Simple (App (App), AppView, Transition (Exit),
                                      run)
import GI.Gtk.Declarative.App.Simple qualified

type State = ()

type Event = ()

view :: State -> AppView Gtk.Window Event
view () =
    bin Gtk.Window
        [#title := "Starway Wallet", on #deleteEvent $ const (True, ())]
        (widget Gtk.Label [#label := "Hello world"])

update :: State -> Event -> Transition State Event
update () () = Exit

main :: IO ()
main = run App{update, view, inputs = [], initialState = ()}
